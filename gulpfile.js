﻿'use strict';

// Definição de pacotes.
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

// Define browsers suportados.
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

// Tarefa para minificação do arquivo site.css.
gulp.task('styles', function () {

  return gulp.src('./wwwroot/css/site.css')
    // Define browsers compatíveis.
    .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
    //Minifica arquivo
    .pipe(csso())
    // Gera o novo arquivo.
    .pipe(gulp.dest('./wwwroot/css/dist/'));
});

function defaultTask(cb) {
  cb();
}

exports.default = defaultTask